@extends('dashboard.layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading mt-4">


    </div>
    <div class="container-fluid white-bg pb-5 mt-5">
        <div class="row">

            <div class="col-md-12">




                <h2 class="text-center pt-2 pb-3">Նոր Դիմորդ</h2>

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <form action="{{route('store')}}" method="post" class="form-horizontal"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}


                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Կնքման Վայրը:</div>
                        <div class="col-sm-6">
                            <input type='text' name="place" class='form-control' placeholder=''>
                            {!! $errors->first('place', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Ամսաթիվ:</div>
                        <div class="col-sm-6">
                            <input   id="datepicker" name="date" class='form-control' placeholder=''>
                            {!! $errors->first('date', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Անուն:</div>
                        <div class="col-sm-6">
                            <input type='text' name="name" class='form-control' placeholder=''>
                            {!! $errors->first('name', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Ազգանուն:</div>
                        <div class="col-sm-6">
                            <input type='text' name="surname" class='form-control' placeholder=''>
                            {!! $errors->first('surname', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Հայրանուն:</div>
                        <div class="col-sm-6">
                            <input type='text' name="third" class='form-control' placeholder=''>
                            {!! $errors->first('third', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Անձնագրի Սերիա:</div>
                        <div class="col-sm-6">
                            <input type='text' name="passport_seria" class='form-control' placeholder=''>
                            {!! $errors->first('passport_seria', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Անձնագրի Տրման Ամսաթիվ:</div>
                        <div class="col-sm-6">
                            <input  name="passport_date" class='form-control' placeholder=''>
                            {!! $errors->first('passport_date', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Անձնագիրը Տրվել է ում կողմից:</div>
                        <div class="col-sm-6">
                            <input type='text' name="passport_koxmic" class='form-control' placeholder=''>
                            {!! $errors->first('passport_koxmic', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Հասցե:</div>
                        <div class="col-sm-6">
                            <input type='text' name="address" class='form-control' placeholder=''>
                            {!! $errors->first('address', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Հեռախոս:</div>
                        <div class="col-sm-6">
                            <input type='text' name="phone" class='form-control' placeholder=''>
                            {!! $errors->first('phone', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Գնված Ապրանքի Անվանում:</div>
                        <div class="col-sm-6">
                            <input type='text' name="apranq_name" class='form-control' placeholder=''>
                            {!! $errors->first('apranq_name', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Չափման Միավոր:</div>
                        <div class="col-sm-6">
                            <input type='text' name="chap" class='form-control' placeholder=''>
                            {!! $errors->first('chap', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Քանակ:</div>
                        <div class="col-sm-6">
                            <input type='text' name="qanak" class='form-control' placeholder=''>
                            {!! $errors->first('qanak', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 control-label font-weight-bold">Գին:</div>
                        <div class="col-sm-6">
                            <input type='text' name="gin" class='form-control' placeholder=''>
                            {!! $errors->first('gin', '<p style="color:red" class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Գումար:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="gumar" class='form-control' placeholder=''>--}}
                            {{--{!! $errors->first('gumar', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Թղթակցող Հաշիվ:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="txtakcox_hashiv" class='form-control' placeholder=''>--}}
                            {{--{!! $errors->first('txtakcox_hashiv', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Վերլուծական հաշվառման ծածկագիրը:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="hashvarman_cackagir" class='form-control' placeholder=''>--}}
                            {{--{!! $errors->first('hashvarman_cackagir', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Նպատակային նշանակության ծածկագիրը:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="nshanakutyan_cackagir" class='form-control' placeholder=''>--}}
                            {{--{!! $errors->first('nshanakutyan_cackagir', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Հիմքը:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="himqy" class='form-control' placeholder='' maxlength="150">--}}
                            {{--{!! $errors->first('himqy', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Հավելված:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="havelvac" class='form-control' placeholder='' maxlength="140">--}}
                            {{--{!! $errors->first('havelvac', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-3 control-label font-weight-bold">Այլ Տեղեկություններ:</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input type='text' name="ayl_texekutyunner" class='form-control' placeholder='' maxlength="130">--}}
                            {{--{!! $errors->first('ayl_texekutyunner', '<p style="color:red" class="help-block">:message</p>') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <input type="submit"  class="btn pull-right btn-success"
                               value="Ստեղծել">
                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
    <script src="/js/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="/js/jquery-ui.js"></script>
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
        });
    </script>
    <script>
        $(function () {
            $('input[name="date"]').daterangepicker({

                autoApply: false,
                locale: {
                    format: 'DD-MM-YYYY'
                },
                opens: 'left',
                singleDatePicker: true,



            });



        }); $(function () {
            $('input[name="passport_date"]').daterangepicker({

                autoApply: false,
                locale: {
                    format: 'DD-MM-YYYY'
                },
                opens: 'left',
                singleDatePicker: true,



            });



        });
    </script>

@endsection
