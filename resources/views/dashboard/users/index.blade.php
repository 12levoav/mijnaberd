@extends('dashboard.layouts.app')

@section('content')

    <div class="container-fluid white-bg pb-5 mt-5">
        <div class="row">

            <div class="col-md-12">


                <h1>Ադմինիստրատորներ</h1>
                <div class="table-responsive">
                    <table class="table table-bordered mt-5">
                        <thead>
                        <tr>

                            <th>Անուն</th>
                            <th>Էլեկտրոնային Հասցե</th>
                            {{--<th>Գործողություններ</th>--}}

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td scope="row">{{$user->name}}</td>
                                <td scope="row">{{$user->email}}</td>


                                {{--<td>--}}
                                    {{--<div class="btn-group my-btn-group">--}}



                                            {{--<a href="{{ route('users.edit', $user->id) }}">--}}
                                                {{--<i class="fa fa-pencil-square-o"></i>--}}
                                            {{--</a>--}}


                                        {{--<form method="POST" action="{{ route('users.destroy',  $user->id) }}"--}}
                                              {{--accept-charset="UTF-8">--}}
                                            {{--{{ method_field('DELETE') }}--}}
                                            {{--{{ csrf_field() }}--}}
                                            {{--<button type="submit">--}}
                                                {{--<i id="delete-id" data-toggle="confirmation" class="fa fa-trash-o"--}}
                                                   {{--aria-hidden="true">--}}
                                                {{--</i>--}}
                                            {{--</button>--}}

                                        {{--</form>--}}


                                    {{--</div>--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <h1>Վաճառողներ</h1>
                <div class="table-responsive">
                    <table class="table table-bordered mt-5">
                        <thead>
                        <tr>

                            <th>ID</th>
                            <th>Անուն</th>
                            <th>Ազգանուն</th>
                            <th>Անձնագրի Սերիա</th>
                            <th>Պայմանագրի Տարեթիվ</th>
                            <th>Հեռախոս</th>
                            <th>Մրգի Պայմանագիր</th>
                            <th>Գնման Ակտ</th>
                            <th>Ելքի Օրդեր</th>
                            <th>Գործողություններ</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $customer)
                            <tr>
                                <td scope="row">{{$customer->id}}</td>
                                <td scope="row">{{$customer->name}}</td>
                                <td scope="row">{{$customer->surname}}</td>
                                <td scope="row">{{$customer->passport_seria}}</td>
                                <td scope="row">{{$customer->date}}</td>
                                <td scope="row">{{$customer->phone}}</td>
                                <td scope="row"><a href="/storage/{{$customer->mrgi_paymanagir}}">Ներբեռնել Մրգի Պայմանագիր</a></td>
                                <td scope="row"> <a href="/storage/{{$customer->gnman_akt}}">Ներբեռնել Գնման Ակտ</a></td>
                                <td scope="row"> <a href="/storage/{{$customer->eo}}">Ներբեռնել Ելքի Օրդերը</a></td>



                                <td>
                                    <div class="btn-group my-btn-group">


                                        <a href="{{URL::to('dashboard/contract/' .$customer->id)}}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                        </a>
                                        {{--<a href="/dashboard/contract/zip/{{$customer->id}}">--}}
                                          {{--Ներբեռնել Բոլորը Zip--}}

                                        {{--</a>--}}

                                        <form method="POST" action="{{ route('customers.destroy',  $customer->id) }}"
                                              accept-charset="UTF-8">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit">
                                                <i id="delete-id" data-toggle="confirmation" class="fa fa-trash-o"
                                                   aria-hidden="true">
                                                </i>
                                            </button>

                                        </form>


                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pull-right pt-2">
                    {{$customers->links()}}
                </div>
            </div>

        </div>


    </div>
@endsection
@section('scripts')

    <script src="/js/bootstrap-confirmation.js"></script>
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
        });
    </script>

@endsection
