@extends('dashboard.layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('content')

    <div class="container-fluid white-bg pb-5 mt-5">
        <div class="row">

            <div class="col-md-12">

                @if(\Illuminate\Support\Facades\Request::route()->uri == 'dashboard/adminsEdit/{id}/edit')

                    <a href="{{ route('admins') }}" title="Վերադառնալ"
                       class="add btn btn-outline btn-success mt-2"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Վերադառնալ

                    </a>
                @else
                    <a href="{{ route('users.index') }}" title="Վերադառնալ"
                       class="add btn btn-outline btn-success mt-2"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Վերադառնալ

                    </a>


                @endif

                <h2 class="text-center pt-2 pb-3"> </h2>
                <form action="{{route('users.update',$user->id)}}" method="post" class="form-horizontal"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            @if(\Illuminate\Support\Facades\Request::route()->uri !== 'dashboard/adminsEdit/{id}/edit')
                            <div class="form-group row">
                                <div class="col-sm-3 control-label font-weight-bold">Անձի Տեսակ:</div>
                                <div class="col-sm-6">
                                    <select class="form-control" name="type" id="type">
                                        <option value="iravaban" @if($user->type == "iravaban") selected @endif>
                                            Իրավաբանական
                                        </option>
                                        <option value="anhat" @if($user->type == "anhat") selected @endif>Անհատ Ձեռնարկատեր</option>
                                        <option value="fizikakan" @if($user->type == "fizikakan") selected @endif>
                                            Ֆիզիկական
                                        </option>
                                    </select>
                                    {!! $errors->first('type', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3 control-label font-weight-bold">Անձնագիր:</div>

                                <div class="col-sm-6">
                                @if($user->files)

                                    @foreach($user->files as $file)
                                        <a href="/storage/{{$file->passport}}" download class="mb-1 d-block">Անձնագիր   {{ $loop->iteration }}</a>
                                    @endforeach
                                @endif
                                <input id="passport" type="file" class="form-control" name="passport[]"  placeholder="{{trans('register.passport')}}" multiple>

                            </div>
                            </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 control-label font-weight-bold">Հեռախոսահամար:</div>
                                    <div class="col-sm-6">
                                        <input type='text' id="phone" name="phone" class='form-control' placeholder='12 123-456'
                                               value="{{$user->details->phone}}">
                                        {!! $errors->first('phone', '<p style="color:red" class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 control-label font-weight-bold">Հասցե:</div>
                                    <div class="col-sm-6">
                                        <input type='text' name="address" class='form-control' placeholder='Հասցե'
                                               value="{{$user->details->address}}">
                                        {!! $errors->first('address', '<p style="color:red" class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <div class="col-sm-3 control-label font-weight-bold">Անուն:</div>
                                <div class="col-sm-6">
                                    <input type='text' name="first_name" class='form-control'
                                           placeholder='Անուն Ազգանուն' value="{{$user->first_name}}">
                                    {!! $errors->first('first_name', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3 control-label font-weight-bold">Էլեկտրոնային Հասցե:</div>
                                <div class="col-sm-6">
                                    <input type='text' name="email" class='form-control'
                                           placeholder='Էլեկտրոնային Հասցե' value="{{$user->email}}">
                                    {!! $errors->first('email', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3 control-label font-weight-bold">Գաղտնաբառ:</div>
                                <div class="col-sm-6">
                                    <input type='password' name="password" class='form-control'
                                           autocomplete="new-password" placeholder='Գաղտնաբառ'>
                                    {!! $errors->first('password', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            @if(\Illuminate\Support\Facades\Request::route()->uri == 'dashboard/adminsEdit/{id}/edit')

                            <div class="form-group row">
                                <div class="col-sm-3 control-label font-weight-bold">Դեր:</div>
                                <div class="col-sm-6">
                                    {!! Form::select('role_id', $roles, isset($user->roles[0]->id) ? $user->roles[0]->id : null, ['class' => 'form-control']) !!}

                                    {!! $errors->first('role_id', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            @else
                                <input type="hidden" name="role_id" value="6">
                            @endif
                                @if(\Illuminate\Support\Facades\Request::route()->uri !== 'dashboard/adminsEdit/{id}/edit')
                            <div class="form-group row changing-line ">
                                <div class="col-sm-3 control-label font-weight-bold">Կազմակերպության Անուն:</div>
                                <div class="col-sm-6">
                                    <input type='text' name="company_name" class='form-control'
                                           placeholder='Կազմակերպության Անուն' value="{{$user->details->company_name}}">
                                    {!! $errors->first('company_name', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group row changing-line">
                                <div class="col-sm-3 control-label font-weight-bold">Պաշտոն:</div>
                                <div class="col-sm-6">
                                    <input type='text' name="position" class='form-control' placeholder='Պաշտոն'
                                           value="{{$user->details->position}}">
                                    {!! $errors->first('position', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group row changing-line">
                                <div class="col-sm-3 control-label font-weight-bold">Գրանցման Համար:</div>
                                <div class="col-sm-6">
                                    <input type='text' name="register_num" class='form-control'
                                           placeholder='Գրանցման Համար' value="{{$user->details->register_num}}">
                                    {!! $errors->first('register_num', '<p style="color:red" class="help-block">:message</p>') !!}
                                </div>
                            </div>
                                @endif
                            <div class="form-group">
                                <input type="submit" class="btn btn-success pull-right" value="Փոփոխել">
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('#phone').mask('+374 00 000-000');
            var old_type = "<?php echo $user->type; ?>";

            if (old_type == "fizikakan") {
                $(".changing-line").children().hide();
            }
        });



        $('#type').on('change', function () {
            var type = $(this).val();
            if (type == 'fizikakan') {

                $(".changing-line").children().hide();
            }
            else {
                $(".changing-line").children().show();
            }
        });
    </script>
@endsection

