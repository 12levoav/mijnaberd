<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <style type="text/css">* {
            font-family: DejaVu Sans !important;
        }</style>
</head>
<body >












<!--[if gte mso 9]><xml>
    <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml><![endif]-->

<!--[if gte mso 9]><xml>
    <w:WordDocument>
        <w:View>Normal</w:View>
        <w:Zoom>0</w:Zoom>
        <w:TrackMoves/>
        <w:TrackFormatting/>
        <w:PunctuationKerning/>
        <w:ValidateAgainstSchemas/>
        <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
        <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
        <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
        <w:DoNotPromoteQF/>
        <w:LidThemeOther>EN-US</w:LidThemeOther>
        <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
        <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
        <w:Compatibility>
            <w:BreakWrappedTables/>
            <w:SnapToGridInCell/>
            <w:WrapTextWithPunct/>
            <w:UseAsianBreakRules/>
            <w:DontGrowAutofit/>
            <w:SplitPgBreakAndParaMark/>
            <w:EnableOpenTypeKerning/>
            <w:DontFlipMirrorIndents/>
            <w:OverrideTableStyleHps/>
        </w:Compatibility>
        <m:mathPr>
            <m:mathFont m:val="Cambria Math"/>
            <m:brkBin m:val="before"/>
            <m:brkBinSub m:val="&#45;-"/>
            <m:smallFrac m:val="off"/>
            <m:dispDef/>
            <m:lMargin m:val="0"/>
            <m:rMargin m:val="0"/>
            <m:defJc m:val="centerGroup"/>
            <m:wrapIndent m:val="1440"/>
            <m:intLim m:val="subSup"/>
            <m:naryLim m:val="undOvr"/>
        </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
    <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
                    DefSemiHidden="false" DefQFormat="false" DefPriority="99"
                    LatentStyleCount="382">
        <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 9"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 1"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 2"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 3"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 4"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 5"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 6"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 7"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 8"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 9"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Normal Indent"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="footnote text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="annotation text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="header"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="footer"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index heading"/>
        <w:LsdException Locked="false" Priority="35" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="caption"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="table of figures"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="envelope address"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="envelope return"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="footnote reference"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="annotation reference"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="line number"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="page number"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="endnote reference"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="endnote text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="table of authorities"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="macro"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="toa heading"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 5"/>
        <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Closing"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Signature"/>
        <w:LsdException Locked="false" Priority="1" SemiHidden="true"
                        UnhideWhenUsed="true" Name="Default Paragraph Font"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text Indent"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Message Header"/>
        <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Salutation"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Date"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text First Indent"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text First Indent 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Heading"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text Indent 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text Indent 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Block Text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Hyperlink"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="FollowedHyperlink"/>
        <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
        <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Document Map"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Plain Text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="E-mail Signature"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Top of Form"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Bottom of Form"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Normal (Web)"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Acronym"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Address"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Cite"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Code"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Definition"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Keyboard"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Preformatted"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Sample"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Typewriter"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Variable"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Normal Table"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="annotation subject"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="No List"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Outline List 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Outline List 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Outline List 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Simple 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Simple 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Simple 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Colorful 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Colorful 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Colorful 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table 3D effects 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table 3D effects 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table 3D effects 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Contemporary"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Elegant"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Professional"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Subtle 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Subtle 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Web 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Web 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Web 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Balloon Text"/>
        <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Theme"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 9"/>
        <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
        <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
        <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
        <w:LsdException Locked="false" Priority="34" QFormat="true"
                        Name="List Paragraph"/>
        <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
        <w:LsdException Locked="false" Priority="30" QFormat="true"
                        Name="Intense Quote"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
        <w:LsdException Locked="false" Priority="19" QFormat="true"
                        Name="Subtle Emphasis"/>
        <w:LsdException Locked="false" Priority="21" QFormat="true"
                        Name="Intense Emphasis"/>
        <w:LsdException Locked="false" Priority="31" QFormat="true"
                        Name="Subtle Reference"/>
        <w:LsdException Locked="false" Priority="32" QFormat="true"
                        Name="Intense Reference"/>
        <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
        <w:LsdException Locked="false" Priority="37" SemiHidden="true"
                        UnhideWhenUsed="true" Name="Bibliography"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
        <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
        <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
        <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
        <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
        <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
        <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
        <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
        <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
        <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 1"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 2"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 3"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 4"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 5"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 6"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 6"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 6"/>
        <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
        <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
        <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 1"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 2"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 3"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 4"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 5"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 6"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 6"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Mention"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Smart Hyperlink"/>
    </w:LatentStyles>
</xml><![endif]-->
<style>
    <!--
    /* Font Definitions */
    @font-face
    {font-family:"Cambria Math";
        panose-1:2 4 5 3 5 4 6 3 2 4;
        mso-font-charset:1;
        mso-generic-font-family:roman;
        mso-font-format:other;
        mso-font-pitch:variable;
        mso-font-signature:0 0 0 0 0 0;}
    @font-face
    {font-family:Calibri;
        panose-1:2 15 5 2 2 2 4 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-536870145 1073786111 1 0 415 0;}
    @font-face
    {font-family:Tahoma;
        panose-1:2 11 6 4 3 5 4 4 2 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
    /* Style Definitions */
    p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0cm;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:12.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    .MsoChpDefault
    {mso-style-type:export-only;
        mso-default-props:yes;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    @page WordSection1
    {size:595.0pt 842.0pt;
        margin:72.0pt 72.0pt 72.0pt 72.0pt;
        mso-header-margin:35.4pt;
        mso-footer-margin:35.4pt;
        mso-paper-source:0;}
    div.WordSection1
    {page:WordSection1;}
    -->
</style>
<!--[if gte mso 10]>
<style>
    /* Style Definitions */
    table.MsoNormalTable
    {mso-style-name:"Table Normal";
        mso-tstyle-rowband-size:0;
        mso-tstyle-colband-size:0;
        mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-parent:"";
        mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
        mso-para-margin:0cm;
        mso-para-margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:12.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;}
</style>
<![endif]-->



<!--StartFragment-->

<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="414" style="width:413.8pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
    <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:10.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="45" nowrap="" valign="bottom" style="width:45.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="3" nowrap="" valign="bottom" style="width:3.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="8" nowrap="" valign="bottom" style="width:8.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="5" nowrap="" valign="bottom" style="width:5.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="29" nowrap="" valign="bottom" style="width:29.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="9" nowrap="" valign="bottom" style="width:9.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="14" nowrap="" valign="bottom" style="width:14.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="7" nowrap="" valign="bottom" style="width:7.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="36" nowrap="" valign="bottom" style="width:36.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="27" nowrap="" valign="bottom" style="width:27.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="16" nowrap="" valign="bottom" style="width:16.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="15" nowrap="" valign="bottom" style="width:15.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="15" nowrap="" valign="bottom" style="width:15.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="8" nowrap="" valign="bottom" style="width:8.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="61" nowrap="" valign="bottom" style="width:61.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="3" nowrap="" valign="bottom" style="width:3.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="6" nowrap="" valign="bottom" style="width:6.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="9" nowrap="" valign="bottom" style="width:9.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="3" nowrap="" valign="bottom" style="width:3.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="37" nowrap="" valign="bottom" style="width:37.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="44" nowrap="" valign="bottom" style="width:44.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:1;height:16.5pt">
        <td width="414" colspan="23" style="width:413.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:2;height:20.25pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.25pt"></td>
        <td width="319" colspan="19" valign="bottom" style="width:319.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.25pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">Միջնաբերդ Ալկո ՍՊԸ<o:p></o:p></span></p>
        </td>
        <td width="37" valign="top" style="width:37.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.25pt">
            <p class="MsoNormal"><b><i><u><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Ձև 2<o:p></o:p></span></u></i></b></p>
        </td>
        <td width="44" valign="top" style="width:44.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.25pt">
            <p class="MsoNormal"><b><i><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">(ԴՐ-2)<o:p></o:p></span></i></b></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.25pt"></td>
    </tr>
    <tr style="mso-yfti-irow:3;height:15.6pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.6pt"></td>
        <td width="319" colspan="19" valign="top" style="width:319.0pt;border:none;
  border-top:dotted black 1.0pt;mso-border-top-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.6pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">կազմակերպության անվանումը<o:p></o:p></span></p>
        </td>
        <td width="81" colspan="2" valign="top" style="width:81.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.6pt"></td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.6pt"></td>
    </tr>
    <tr style="mso-yfti-irow:4;height:18.0pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt"></td>
        <td width="229" colspan="13" valign="bottom" style="width:229.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt">
            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:11.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">Դրամարկղային ելքի օրդեր N<o:p></o:p></span></b></p>
        </td>
        <td width="78" colspan="4" valign="bottom" style="width:78.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.0pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="9" valign="top" style="width:9.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt"></td>
        <td width="84" colspan="3" valign="bottom" style="width:84.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.0pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black"><o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt"></td>
    </tr>
    <tr style="mso-yfti-irow:5;height:15.0pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.0pt"></td>
        <td width="307" colspan="17" style="width:307.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.0pt"></td>
        <td width="9" valign="top" style="width:9.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.0pt"></td>
        <td width="84" colspan="3" valign="top" style="width:84.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.0pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">կազմման ամսաթիվ<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:15.0pt"></td>
    </tr>
    <tr style="mso-yfti-irow:6;height:37.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:37.5pt"></td>
        <td width="90" colspan="5" valign="top" style="width:90.0pt;border:dotted black 1.0pt;
  mso-border-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:37.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:9.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">Թղթակցող հաշիվը<o:p></o:p></span></p>
        </td>
        <td width="93" colspan="5" valign="top" style="width:93.0pt;border:dotted black 1.0pt;
  border-left:none;mso-border-top-alt:dotted black .5pt;mso-border-bottom-alt:
  dotted black .5pt;mso-border-right-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:37.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:9.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">Վերլուծական հաշվառման ծածկագիրը<o:p></o:p></span></p>
        </td>
        <td width="115" colspan="5" valign="top" style="width:115.0pt;border:dotted black 1.0pt;
  border-left:none;mso-border-top-alt:dotted black .5pt;mso-border-bottom-alt:
  dotted black .5pt;mso-border-right-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:37.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:9.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">Գումարը<o:p></o:p></span></p>
        </td>
        <td width="102" colspan="6" valign="top" style="width:102.0pt;border:dotted black 1.0pt;
  border-left:none;mso-border-top-alt:dotted black .5pt;mso-border-bottom-alt:
  dotted black .5pt;mso-border-right-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:37.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:9.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">Նպատակային նշանակության ծածկագիրը<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:37.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:7;height:18.75pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.75pt"></td>
        <td width="90" colspan="5" style="width:90.0pt;border:dotted black 1.0pt;
  border-top:none;mso-border-top-alt:dotted black .5pt;mso-border-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">1<o:p></o:p></span></p>
        </td>
        <td width="93" colspan="5" style="width:93.0pt;border-top:none;border-left:none;
  border-bottom:dotted black 1.0pt;border-right:dotted black 1.0pt;mso-border-top-alt:
  dotted black .5pt;mso-border-top-alt:dotted black .5pt;mso-border-bottom-alt:
  dotted black .5pt;mso-border-right-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">2<o:p></o:p></span></p>
        </td>
        <td width="115" colspan="5" style="width:115.0pt;border-top:none;border-left:
  none;border-bottom:dotted black 1.0pt;border-right:dotted black 1.0pt;
  mso-border-top-alt:dotted black .5pt;mso-border-top-alt:dotted black .5pt;
  mso-border-bottom-alt:dotted black .5pt;mso-border-right-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">3<o:p></o:p></span></p>
        </td>
        <td width="102" colspan="6" style="width:102.0pt;border-top:none;border-left:
  none;border-bottom:dotted black 1.0pt;border-right:dotted black 1.0pt;
  mso-border-top-alt:dotted black .5pt;mso-border-top-alt:dotted black .5pt;
  mso-border-bottom-alt:dotted black .5pt;mso-border-right-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">4<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.75pt"></td>
    </tr>
    <tr style="mso-yfti-irow:8;height:19.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:19.5pt"></td>
        <td width="90" colspan="5" valign="bottom" style="width:90.0pt;border:dotted black 1.0pt;
  border-top:none;mso-border-left-alt:dotted black .5pt;mso-border-bottom-alt:
  dotted black .5pt;mso-border-right-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:19.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">&nbsp;{{$customer['txtakcox_hashiv']}}<o:p></o:p></span></p>
        </td>
        <td width="93" colspan="5" valign="bottom" style="width:93.0pt;border-top:none;
  border-left:none;border-bottom:dotted black 1.0pt;border-right:dotted black 1.0pt;
  mso-border-bottom-alt:dotted black .5pt;mso-border-right-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:19.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">&nbsp;{{$customer['hashvarman_cackagir']}}<o:p></o:p></span></p>
        </td>
        <td width="115" colspan="5" valign="bottom" style="width:115.0pt;border-top:none;
  border-left:none;border-bottom:dotted black 1.0pt;border-right:dotted black 1.0pt;
  mso-border-bottom-alt:dotted black .5pt;mso-border-right-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:19.5pt">
            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">{{$customer['gumar']}}&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="102" colspan="6" valign="bottom" style="width:102.0pt;border-top:none;
  border-left:none;border-bottom:dotted black 1.0pt;border-right:dotted black 1.0pt;
  mso-border-bottom-alt:dotted black .5pt;mso-border-right-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:19.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:10.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">{{$customer['nshanakutyan_cackagir']}}&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:19.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:9;height:18.0pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt"></td>
        <td width="56" colspan="3" valign="bottom" style="width:56.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Ստացող<o:p></o:p></span></p>
        </td>
        <td width="344" colspan="18" valign="bottom" style="width:344.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.0pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;{{$customer['name']}} {{$customer['surname']}}<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:18.0pt"></td>
    </tr>
    <tr style="mso-yfti-irow:10;height:9.75pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="48" colspan="2" valign="top" style="width:48.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="352" colspan="19" valign="top" style="width:352.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">անուն, ազգանունը<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
    </tr>
    <tr style="mso-yfti-irow:11;height:16.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="56" colspan="3" valign="bottom" style="width:56.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Հիմքը <o:p></o:p></span></p>
        </td>
        <td width="344" colspan="18" valign="bottom" style="width:344.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black"><span style="  display:block;
    width:420;
    word-wrap:break-word;
 position: absolute;
    top: -22px;


">&nbsp;{{$customer['himqy']}}</span><o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:12;height:11.25pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
        <td width="400" colspan="21" valign="top" style="width:400.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:11.25pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
    </tr>
    <tr style="mso-yfti-irow:13;height:17.25pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt"></td>
        <td width="56" colspan="3" valign="bottom" style="width:56.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Գումարը<o:p></o:p></span></p>
        </td>
        <td width="344" colspan="18" valign="bottom" style="width:344.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:17.25pt">
            <p class="MsoNormal"><i><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></i></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt"></td>
    </tr>
    <tr style="mso-yfti-irow:14;height:10.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="400" colspan="21" valign="top" style="width:400.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">տառերով<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:15;height:10.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="45" valign="top" style="width:45.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="3" valign="top" style="width:3.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="8" valign="top" style="width:8.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="5" valign="top" style="width:5.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="29" nowrap="" valign="bottom" style="width:29.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="9" valign="top" style="width:9.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="14" valign="top" style="width:14.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="7" valign="top" style="width:7.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="36" valign="top" style="width:36.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="27" valign="top" style="width:27.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="16" valign="top" style="width:16.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="15" valign="top" style="width:15.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="15" valign="top" style="width:15.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="8" valign="top" style="width:8.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="61" valign="top" style="width:61.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="3" valign="top" style="width:3.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="6" valign="top" style="width:6.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="9" valign="top" style="width:9.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="3" valign="top" style="width:3.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="37" valign="top" style="width:37.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="44" valign="top" style="width:44.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:16;height:16.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="61" colspan="4" valign="bottom" style="width:61.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Հավելված<o:p></o:p></span></p>
        </td>
        <td width="339" colspan="17" valign="bottom" style="width:339.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black"><span style=" display:block;
    width:420;
    word-wrap:break-word;
 position: absolute;
    top: -22px;


">&nbsp;{{$customer['havelvac']}}</span><o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:12;height:11.25pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
        <td width="400" colspan="21" valign="top" style="width:400.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:11.25pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
    </tr>
    <tr style="mso-yfti-irow:18;height:28.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.5pt"></td>
        <td width="113" colspan="7" valign="bottom" style="width:113.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Կազմակերպության
  ղեկավար<o:p></o:p></span></p>
        </td>
        <td width="101" colspan="5" valign="top" style="width:101.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.5pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="87" colspan="4" valign="bottom" style="width:87.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Գլխավոր հաշվապահ<o:p></o:p></span></p>
        </td>
        <td width="99" colspan="5" valign="top" style="width:99.0pt;border:none;border-bottom:
  dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.5pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:19;height:10.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="113" colspan="7" valign="top" style="width:113.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="101" colspan="5" valign="top" style="width:101.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">ստորագրություն<o:p></o:p></span></p>
        </td>
        <td width="87" colspan="4" valign="top" style="width:87.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
        <td width="99" colspan="5" valign="top" style="width:99.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">ստորագրություն<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:10.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:20;height:16.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="56" colspan="3" valign="bottom" style="width:56.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Ստացա<o:p></o:p></span></p>
        </td>
        <td width="344" colspan="18" valign="bottom" style="width:344.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:21;height:9.75pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="400" colspan="21" valign="top" style="width:400.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
    </tr>
    <tr style="mso-yfti-irow:22;height:9.75pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="400" colspan="21" valign="top" style="width:400.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">տառերով<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
    </tr>
    <tr style="mso-yfti-irow:23;height:17.25pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt"></td>
        <td width="45" nowrap="" valign="bottom" style="width:45.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">«<span style="mso-spacerun:yes">&nbsp;&nbsp; </span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>»<o:p></o:p></span></p>
        </td>
        <td width="54" colspan="5" valign="top" style="width:54.0pt;border:none;border-bottom:
  solid black 1.0pt;mso-border-bottom-alt:solid black .5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="138" colspan="8" valign="bottom" style="width:138.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">2019թ.<o:p></o:p></span></p>
        </td>
        <td width="163" colspan="7" valign="bottom" style="width:163.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:17.25pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.25pt"></td>
    </tr>
    <tr style="mso-yfti-irow:24;height:9.75pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="99" colspan="6" valign="top" style="width:99.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="100" colspan="5" valign="top" style="width:100.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
        <td width="201" colspan="10" valign="top" style="width:201.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">ստորագրություն<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.75pt"></td>
    </tr>
    <tr style="mso-yfti-irow:25;height:16.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="120" colspan="8" valign="bottom" style="width:120.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Այլ տեղեկություններ<o:p></o:p></span></p>
        </td>
        <td width="280" colspan="13" valign="bottom" style="width:280.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black"><span style=" display:block;
    width:325;
    word-wrap:break-word;
 position: absolute;
    top: -22px;


">&nbsp;{{$customer['ayl_texekutyunner']}}</span><o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:26;height:12.75pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:12.75pt"></td>
        <td width="400" colspan="21" valign="top" style="width:400.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:12.75pt"></td>
    </tr>
    <tr style="mso-yfti-irow:27;height:16.5pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="99" colspan="6" valign="top" style="width:99.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="57" colspan="3" valign="top" style="width:57.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
        <td width="73" colspan="4" valign="bottom" style="width:73.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt">
            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">Գանձապահ<o:p></o:p></span></p>
        </td>
        <td width="171" colspan="8" valign="top" style="width:171.0pt;border:none;
  border-bottom:dotted black 1.0pt;mso-border-bottom-alt:dotted black .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt">
            <p class="MsoNormal"><span style="font-size:8.0pt;font-family:Tahoma;
  mso-fareast-font-family:&quot;Times New Roman&quot;;color:black">&nbsp;<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.5pt"></td>
    </tr>
    <tr style="mso-yfti-irow:28;mso-yfti-lastrow:yes;height:11.25pt">
        <td width="2" nowrap="" valign="bottom" style="width:1.8pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
        <td width="99" colspan="6" valign="top" style="width:99.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
        <td width="100" colspan="5" valign="top" style="width:100.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
        <td width="30" colspan="2" valign="top" style="width:30.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
        <td width="171" colspan="8" valign="top" style="width:171.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt">
            <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:7.0pt;font-family:Tahoma;mso-fareast-font-family:&quot;Times New Roman&quot;;
  color:black">ստորագրություն<o:p></o:p></span></p>
        </td>
        <td width="12" nowrap="" valign="bottom" style="width:12.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:11.25pt"></td>
    </tr>
    </tbody></table>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<!--EndFragment-->
</body>
</html>


