<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <style type="text/css">* {
            font-family: DejaVu Sans !important;
        }</style>
</head>
<body >




<em style="font-size: 9pt;border-bottom: 0.5px solid black;">&lt;&lt;ՄԻՋՆԱԲԵՐԴ ԱԼԿՈ&gt;&gt;</em>
<p style="font-size: 9pt;float:right">Հաստատում եմ </p>
<br>
<br>
<p style="font-size: 9pt;float:right;margin-left:170px">Տնօրեն      Գ. Ավետիսյան</p>
<br>
<br>
<br>
<p style="font-size: 9pt;float:right;margin-left:170px">_____________2019թ.</p>
<br>
<br>
<br>

<p style="font-size: 12pt;text-align: center; font-weight: bold">Գնման ակտ  N  </p>

<br>


<br>
<br>
<br>

<br>
<br>
<p style="font-size: 9pt;border-bottom: 0.5px solid black;width: 90%">Մատակարար` {{$customer['name']}} {{$customer['surname']}}</p>
<p style="margin: 0;  font-size: 7px; letter-spacing: 0.1px; ">
    (Ֆիզիկական անձի անուն, ազգանուն)
</p>
<br>

<p style="font-size: 9pt;">Անձնագրի սերիան <em style="font-size: 9pt;border-bottom: 0.5px solid black;">{{$customer['passport_seria']}}</em> տրված <em style="font-size: 9pt;border-bottom: 0.5px solid black;">{{$customer['passport_koxmic']}}</em> կողմից, <em style="font-size: 9pt;border-bottom: 0.5px solid black;">{{$passport}}թ.</em> </p>
<br>
<div style="display:inline">
<p style="font-size: 9pt;">Գնման վայրը  </p>
    <p style="font-size: 9pt;border-bottom: 0.5px solid black;width: 70%">{{$customer['place']}}</p>
</div>
    <br>
<br>
<br>
<table style="border-collapse: collapse; width: 651px; margin: 5px 0 48px 0;">
    <tr style="line-height: 17px; font-size: 12px; height: 42px;">
        <td
            style="border: 1px solid black; height: 17px; padding: 0; margin: 0; width: 26px; text-align: center;"
            width="26" height="17" align="center">N
        </td>
        <td
            style="border: 1px solid black; height: 17px; padding: 0; margin: 0; width: 250px; padding-left: 7px;"
            height="17">Գնված ապրանքի անվանումը
        </td>
        <td
            style="border: 1px solid black; height: 17px; padding: 0; margin: 0; padding-left: 7px;"
            height="17"
            >Չափի
            Միավ.
        </td>
        <td
            style="border: 1px solid black; height: 17px; padding: 0; margin: 0; padding-left: 7px;"
            height="17"

        >Քանակ


        </td>
        <td
            style="border: 1px solid black; height: 17px; padding: 0; margin: 0; padding-left: 7px;"
            height="17"

        >Գին


        </td>
        <td
            style="border: 1px solid black; height: 17px; padding: 0; margin: 0; padding-left: 8px;"
            height="17"

        >Գումար
        </td>
    </tr>


        <tr style="background-color: gray;height: 42px;">
            <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;"
                height="17">1   </td>
            <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;"
                height="17"

            >{{ $customer['apranq_name']}}</td>
            <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;"
                height="17"

            >{{ $customer['chap']}}</td>
            <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;"
                height="17"

            >{{ $customer['qanak']}}</td>
            <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;"
                height="17"

            >
                {{ $customer['gin']}}

            </td>
            <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;"
                height="17"
               >{{ $customer['gin']*$customer['qanak']}} </td>

        </tr>

    <tr style="height: 42px;">
        <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;">

        </td>
        <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;" >
            Ընդամենը
        </td>
        <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;">
            {{ $customer['chap']}}
        </td>  <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;">
            {{ $customer['qanak']}}
        </td>  <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;">
            {{ $customer['gin']}}
        </td>  <td style="border: 1px solid black; height: 17px; padding: 0; margin: 0;">
            {{ $customer['gin']*$customer['qanak']}}
        </td>
    </tr>


</table>

<span style="text-indent: 0; ">Հանձնեց</span>
<span style="text-indent: 0; margin-left:350px;">Ընդունեց</span>
<br>
<br>
<span style="text-indent: 0; ">
                    ______________________

                </span>
<span style="text-indent: 0; margin-left:250px;">
                    ______________________

                </span>
<br>
<br>
<br>
<span style="text-indent: 0; ">{{$customer['name']}} {{$customer['surname']}}</span>
<span style="text-indent: 0; margin-left:290px;">Տնօրեն ` Գ.Ավետիսյան</span>




</body>
</html>


