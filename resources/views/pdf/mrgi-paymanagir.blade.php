<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <style type="text/css">
        * {
            font-family: DejaVu Sans !important;
        }
    </style>

</head>
<body>







<!--[if !mso]>
<style>
    v\:* {behavior:url(#default#VML);}
    o\:* {behavior:url(#default#VML);}
    w\:* {behavior:url(#default#VML);}
    .shape {behavior:url(#default#VML);}
</style>
<![endif]--><!--[if gte mso 9]><xml>
    <o:OfficeDocumentSettings>
        <o:RelyOnVML/>
        <o:AllowPNG/>
    </o:OfficeDocumentSettings>
</xml><![endif]-->

<!--[if gte mso 9]><xml>
    <w:WordDocument>
        <w:View>Normal</w:View>
        <w:Zoom>0</w:Zoom>
        <w:TrackMoves>false</w:TrackMoves>
        <w:TrackFormatting/>
        <w:PunctuationKerning/>
        <w:ValidateAgainstSchemas/>
        <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
        <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
        <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
        <w:DoNotPromoteQF/>
        <w:LidThemeOther>EN-US</w:LidThemeOther>
        <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
        <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
        <w:Compatibility>
            <w:BreakWrappedTables/>
            <w:SnapToGridInCell/>
            <w:WrapTextWithPunct/>
            <w:UseAsianBreakRules/>
            <w:DontGrowAutofit/>
            <w:SplitPgBreakAndParaMark/>
            <w:EnableOpenTypeKerning/>
            <w:DontFlipMirrorIndents/>
            <w:OverrideTableStyleHps/>
        </w:Compatibility>
        <m:mathPr>
            <m:mathFont m:val="Cambria Math"/>
            <m:brkBin m:val="before"/>
            <m:brkBinSub m:val="&#45;-"/>
            <m:smallFrac m:val="off"/>
            <m:dispDef/>
            <m:lMargin m:val="0"/>
            <m:rMargin m:val="0"/>
            <m:defJc m:val="centerGroup"/>
            <m:wrapIndent m:val="1440"/>
            <m:intLim m:val="subSup"/>
            <m:naryLim m:val="undOvr"/>
        </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
    <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
                    DefSemiHidden="false" DefQFormat="false" DefPriority="99"
                    LatentStyleCount="382">
        <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index 9"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 1"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 2"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 3"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 4"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 5"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 6"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 7"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 8"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" Name="toc 9"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Normal Indent"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="footnote text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="annotation text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="header"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="footer"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="index heading"/>
        <w:LsdException Locked="false" Priority="35" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="caption"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="table of figures"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="envelope address"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="envelope return"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="footnote reference"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="annotation reference"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="line number"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="page number"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="endnote reference"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="endnote text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="table of authorities"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="macro"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="toa heading"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Bullet 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Number 5"/>
        <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Closing"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Signature"/>
        <w:LsdException Locked="false" Priority="1" SemiHidden="true"
                        UnhideWhenUsed="true" Name="Default Paragraph Font"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text Indent"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="List Continue 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Message Header"/>
        <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Salutation"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Date"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text First Indent"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text First Indent 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Heading"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text Indent 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Body Text Indent 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Block Text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Hyperlink"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="FollowedHyperlink"/>
        <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
        <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Document Map"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Plain Text"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="E-mail Signature"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Top of Form"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Bottom of Form"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Normal (Web)"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Acronym"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Address"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Cite"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Code"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Definition"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Keyboard"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Preformatted"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Sample"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Typewriter"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="HTML Variable"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Normal Table"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="annotation subject"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="No List"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Outline List 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Outline List 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Outline List 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Simple 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Simple 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Simple 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Classic 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Colorful 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Colorful 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Colorful 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Columns 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Grid 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table List 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table 3D effects 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table 3D effects 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table 3D effects 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Contemporary"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Elegant"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Professional"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Subtle 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Subtle 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Web 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Web 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Web 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Balloon Text"/>
        <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Table Theme"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 1"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 2"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 3"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 4"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 5"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 7"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 8"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Note Level 9"/>
        <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
        <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
        <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
        <w:LsdException Locked="false" Priority="34" QFormat="true"
                        Name="List Paragraph"/>
        <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
        <w:LsdException Locked="false" Priority="30" QFormat="true"
                        Name="Intense Quote"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
        <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
        <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
        <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
        <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
        <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
        <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
        <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
        <w:LsdException Locked="false" Priority="19" QFormat="true"
                        Name="Subtle Emphasis"/>
        <w:LsdException Locked="false" Priority="21" QFormat="true"
                        Name="Intense Emphasis"/>
        <w:LsdException Locked="false" Priority="31" QFormat="true"
                        Name="Subtle Reference"/>
        <w:LsdException Locked="false" Priority="32" QFormat="true"
                        Name="Intense Reference"/>
        <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
        <w:LsdException Locked="false" Priority="37" SemiHidden="true"
                        UnhideWhenUsed="true" Name="Bibliography"/>
        <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                        UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
        <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
        <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
        <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
        <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
        <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
        <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
        <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
        <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
        <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 1"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 2"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 3"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 4"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 5"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="Grid Table 1 Light Accent 6"/>
        <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
        <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="Grid Table 6 Colorful Accent 6"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="Grid Table 7 Colorful Accent 6"/>
        <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
        <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
        <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 1"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 1"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 2"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 2"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 3"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 3"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 4"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 4"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 5"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 5"/>
        <w:LsdException Locked="false" Priority="46"
                        Name="List Table 1 Light Accent 6"/>
        <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
        <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
        <w:LsdException Locked="false" Priority="51"
                        Name="List Table 6 Colorful Accent 6"/>
        <w:LsdException Locked="false" Priority="52"
                        Name="List Table 7 Colorful Accent 6"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Mention"/>
        <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                        Name="Smart Hyperlink"/>
    </w:LatentStyles>
</xml><![endif]-->
<style>
    <!--
    /* Font Definitions */
    @font-face
    {font-family:Sylfaen;
        panose-1:1 10 5 2 5 3 6 3 3 3;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:67110535 0 0 0 159 0;}
    @font-face
    {font-family:"Cambria Math";
        panose-1:2 4 5 3 5 4 6 3 2 4;
        mso-font-charset:1;
        mso-generic-font-family:roman;
        mso-font-format:other;
        mso-font-pitch:variable;
        mso-font-signature:0 0 0 0 0 0;}
    @font-face
    {font-family:Calibri;
        panose-1:2 15 5 2 2 2 4 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-536870145 1073786111 1 0 415 0;}
    /* Style Definitions */
    p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:8.0pt;
        margin-left:0cm;
        line-height:107%;
        mso-pagination:widow-orphan;
        font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
    {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:8.0pt;
        margin-left:36.0pt;
        mso-add-space:auto;
        line-height:107%;
        mso-pagination:widow-orphan;
        font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
    {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:36.0pt;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        line-height:107%;
        mso-pagination:widow-orphan;
        font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
    {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:0cm;
        margin-left:36.0pt;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        line-height:107%;
        mso-pagination:widow-orphan;
        font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
    {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0cm;
        margin-right:0cm;
        margin-bottom:8.0pt;
        margin-left:36.0pt;
        mso-add-space:auto;
        line-height:107%;
        mso-pagination:widow-orphan;
        font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    .MsoChpDefault
    {mso-style-type:export-only;
        mso-default-props:yes;
        font-size:11.0pt;
        mso-ansi-font-size:11.0pt;
        mso-bidi-font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-fareast-font-family:Calibri;
        mso-fareast-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;
        mso-bidi-font-family:"Times New Roman";
        mso-bidi-theme-font:minor-bidi;}
    .MsoPapDefault
    {mso-style-type:export-only;
        margin-bottom:8.0pt;
        line-height:107%;}
    @page WordSection1
    {size:21.0cm 841.95pt;
        margin:72.0pt 49.5pt 72.0pt 72.0pt;
        mso-header-margin:36.0pt;
        mso-footer-margin:36.0pt;
        mso-paper-source:0;}
    div.WordSection1
    {page:WordSection1;}
    /* List Definitions */
    @list l0
    {mso-list-id:993144827;
        mso-list-template-ids:-197080038;}
    @list l0:level1
    {mso-level-tab-stop:none;
        mso-level-number-position:left;
        text-indent:-18.0pt;}
    @list l0:level2
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:36.0pt;
        text-indent:-18.0pt;}
    @list l0:level3
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:54.0pt;
        text-indent:-36.0pt;}
    @list l0:level4
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3\.%4";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:54.0pt;
        text-indent:-36.0pt;}
    @list l0:level5
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3\.%4\.%5";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:72.0pt;
        text-indent:-54.0pt;}
    @list l0:level6
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:72.0pt;
        text-indent:-54.0pt;}
    @list l0:level7
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:90.0pt;
        text-indent:-72.0pt;}
    @list l0:level8
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:90.0pt;
        text-indent:-72.0pt;}
    @list l0:level9
    {mso-level-legal-format:yes;
        mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9";
        mso-level-tab-stop:none;
        mso-level-number-position:left;
        margin-left:90.0pt;
        text-indent:-72.0pt;}
    ol
    {margin-bottom:0cm;}
    ul
    {margin-bottom:0cm;}
    -->
</style>
<!--[if gte mso 10]>
<style>
    /* Style Definitions */
    table.MsoNormalTable
    {mso-style-name:"Table Normal";
        mso-tstyle-rowband-size:0;
        mso-tstyle-colband-size:0;
        mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-parent:"";
        mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
        mso-para-margin-top:0cm;
        mso-para-margin-right:0cm;
        mso-para-margin-bottom:8.0pt;
        mso-para-margin-left:0cm;
        line-height:107%;
        mso-pagination:widow-orphan;
        font-size:11.0pt;
        font-family:Calibri;
        mso-ascii-font-family:Calibri;
        mso-ascii-theme-font:minor-latin;
        mso-hansi-font-family:Calibri;
        mso-hansi-theme-font:minor-latin;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
    <o:shapedefaults v:ext="edit" spidmax="1028"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
    <o:shapelayout v:ext="edit">
        <o:idmap v:ext="edit" data="1"/>
    </o:shapelayout></xml><![endif]-->



<!--StartFragment-->

<p class="MsoNormal" align="center" style="margin-right:-4.5pt;text-align:center"><b style="mso-bidi-font-weight:normal"><span style="font-family:Sylfaen">Մրգի գնման<o:p></o:p></span></b></p>

<p class="MsoNormal" align="center" style="text-align:center;line-height:normal"><b style="mso-bidi-font-weight:normal"><span style="font-family:Sylfaen"><span style="mso-spacerun:yes">&nbsp;</span><span style="mso-spacerun:yes">&nbsp;&nbsp; </span><span style="mso-spacerun:yes">&nbsp;</span>Պայմանագիր N<o:p></o:p></span></b></p>

<p class="MsoNormal" style="line-height:normal;tab-stops:right 468.0pt"><span style="font-size:9.0pt;font-family:Sylfaen"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="  display:block;
    width:250px;
    word-wrap:break-word;
 position: relative;
    top: 15px;


">{{$customer['place']}}</span></span><span style="font-size:10.0pt;font-family:Sylfaen"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="font-size:9.0pt;font-family:Sylfaen;margin-left:200px !important; ">{{$date}}</span><span style="font-size:10.0pt;font-family:Sylfaen"><o:p></o:p></span></p>

<p class="MsoNormal" style="line-height:normal;tab-stops:right 468.0pt"><span style="font-size:9.0pt;font-family:Sylfaen"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>(կնքման վայրը)</span><span style="font-size:10.0pt;font-family:Sylfaen"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="font-size:9.0pt;font-family:Sylfaen">(օր, ամիս, տարի)</span><span style="font-size:10.0pt;font-family:Sylfaen"><o:p></o:p></span></p>

<p class="MsoNormal" style="line-height:normal;tab-stops:right 468.0pt"><span style="font-size:10.0pt;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-left:13.5pt;text-align:justify;text-indent:
18.0pt;tab-stops:right 468.0pt"><span style="font-size:10.0pt;line-height:107%;
font-family:Sylfaen">&lt;&lt;ՄԻՋՆԱԲԵՐԴ ԱԼԿՈ&gt;&gt; ՍՊԸ-ն, այսուհետ՝ &lt;&lt;Գնորդ&gt;&gt;,
ի դեմս Տնօրեն Գագիկ Ավետիսյան, որը գործում է՝ կազմակերպության կանոնադրության հիման
վրա, մի կողմից, {{$customer['name']}} {{$customer['surname']}} , այսուհետ
&lt;&lt;Վաճառող&gt;&gt;, որը հանդիսանում է ֆիզիկական անձ՝ մյուս կողմից, այսուհետ՝
նաև &lt;&lt;Կողմեր&gt;&gt;, իսկ առանձին՝ &lt;&lt;Կողմ&gt;&gt;, պայմանավորվում են
հետևյալի մասին՝<o:p></o:p></span></p>

<p class="MsoListParagraph" align="center" style="text-align:center;text-indent:
-18.0pt;mso-list:l0 level1 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen">Պայմանագրի առարկան<o:p></o:p></span></b></p>

<p class="MsoNormal" style="margin-left:13.5pt;text-align:justify;text-indent:
18.0pt;tab-stops:right 468.0pt"><span style="font-size:10.0pt;line-height:107%;
font-family:Sylfaen">&lt;&lt;Վաճառողը&gt;&gt; պարտավորվում է իր կողմից աճեցված բերքը(միրգը)
(այսուհետ՝ &lt;&lt;Ապրանք&gt;&gt;) սույն պայմանագրով սահմանված պայմաններով վաճառել
&lt;&lt;Գնորդին&gt;&gt;, իսկ &lt;&lt;Գնորդը&gt;&gt; պարտավորվում է գնել այն:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpFirst" align="center" style="text-align:center;
text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen">&lt;&lt;Ապրանքի&gt;&gt;
որակին ներկայացվող պահանջները<o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpLast" style="margin-left:27.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-list:l0 level2 lfo1;tab-stops:22.5pt right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen;mso-fareast-font-family:
Sylfaen;mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">2.1<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen"><span style="mso-spacerun:yes">&nbsp;&nbsp;</span>Սույն պայմանագրով վաճառքի ենթակա &lt;&lt;Ապրանքը&gt;&gt;
գնվելու է բացառապես &lt;&lt;Վաճառողի&gt;&gt; կողմից մշակվող և սույն կետում նշված
հողատարածքից ստացված բերքից, որը հավաստվում է գյուղատնտեսական գործունեությամբ զբաղվելու
փաստը հավաստող տեղական ինքնակառավարման մարմնի կողմից տրված տեղեկանքով:<o:p></o:p></span></p>

<p class="MsoNormal" style="text-indent:9.0pt;tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen">Տեղակայումը՝ <o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpFirst" style="text-align:justify;text-indent:-27.0pt;
mso-list:l0 level2 lfo1;tab-stops:31.5pt right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen;mso-fareast-font-family:
Sylfaen;mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">2.2<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen">Համաձայն սույն պայամանագրի՝
&lt;&lt;Վաճառողը&gt;&gt; պարտավորվում է &lt;&lt;Գնորդին&gt;&gt; <span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>վաճառել, իսկ &lt;&lt;Գնորդը&gt;&gt; պարտավորվում
է Պայմանագրի գործողության ընթացքում &lt;&lt;Վաճառողից&gt;&gt; գնել &lt;&lt;Ապրանք&gt;&gt;,
որի տեսակը, քանակը և արժեքը նշված կլինի համապատասխան գնման ակտում:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-27.0pt;
mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen;mso-fareast-font-family:
Sylfaen;mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">2.3<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen">Սույն պայամանգրով
&lt;&lt;Գնորդը&gt;&gt; պարտավորվում է &lt;&lt;Ապրանքը&gt;&gt; գնել ստորև բերված
պայմաններով.<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-left:45.0pt;mso-add-space:
auto;text-align:justify;text-indent:-36.0pt;mso-list:l0 level3 lfo1;tab-stops:
right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">2.3.1<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;
font-family:Sylfaen">&lt;&lt;Ապրանքը&gt;&gt; պետք է հասունացած լինի:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-left:45.0pt;mso-add-space:
auto;text-align:justify;text-indent:-36.0pt;mso-list:l0 level3 lfo1;tab-stops:
right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">2.3.2<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;
font-family:Sylfaen">&lt;&lt;Ապրանքի&gt;&gt; տարբեր տեսակները (սորտերը) պետք է մատակարարվեն
առանձին:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-left:45.0pt;mso-add-space:
auto;text-align:justify;text-indent:-36.0pt;mso-list:l0 level3 lfo1;tab-stops:
right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">2.3.3<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;
font-family:Sylfaen">&lt;&lt;Ապրանքի&gt;&gt; արտաքին տեսքը պետք է համապատասխանի
տվյալ տեսակների (սորտերի) տեսակային (սորտային) առանձնահատկություններին: <o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-left:45.0pt;mso-add-space:
auto;text-align:justify;text-indent:-36.0pt;mso-list:l0 level3 lfo1;tab-stops:
right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">2.3.4<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:10.0pt;line-height:107%;
font-family:Sylfaen">Մատակարարվող &lt;&lt;Ապրանքը&gt;&gt; պետք է լինի մաքուր, զերծ
լինի օտար մարմիններից (չլինի կեղտոտ, հիվանդոտ):<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;tab-stops:right 468.0pt"><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" align="center" style="text-align:center;
text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:
Sylfaen"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:107%;font-family:Sylfaen">Վաճառքի կարգը և փուլերը<o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;line-height:
107%;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">3.1<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">Վաճառքի ընթացքում &lt;&lt;Վաճառողը&gt;&gt;
պետք է ներկայացնի իր անձնագիրը, սույն պայմանագրի իր օրինակը, տեղական ինքնակառավարման
մարմնի կամ այգեգործական ընկերության կողմից տրամադրվող գյուղատնտեսական արտադրանքի
արտադրությամբ զբաղվելու մասին տեղեկանք:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;line-height:normal;
tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" align="center" style="text-align:center;
text-indent:-18.0pt;line-height:normal;mso-list:l0 level1 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:
Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:Sylfaen">Գինը և վճարման կարգը<o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">4.1<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">&lt;&lt;Ապրանքի&gt;&gt; գինը կարող
է սահմանվել Ապրանքի 1 կգ ֆիզիկական քաշի հաշվով կամ բազիսյին շաքարայնությունից կախված:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">4.2<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">Վճարումը կիրականացվի կանխիկ
&lt;&lt;Գնորդի&gt;&gt; դրամարկղից կամ բանկային փոխանցման միջոցով՝ <b style="mso-bidi-font-weight:normal">մթերումից 5 աշխատանքային օրվա ընթացքում</b>:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;line-height:normal;
tab-stops:right 468.0pt"><span style="font-size:10.0pt;font-family:Sylfaen">Այն
դեպքում, երբ &lt;&lt;Վաճառողի&gt;&gt; կողմից նախատեսված չեն համապատասխան փաստաթղթեր,
վճարումը կատարելու համար, կամ այդ փաստաթղթերը թերի են, սույն կետում նշված ժամկետի
հաշվարկը սկսվում է փաստաթղթերի վերջնական<span style="mso-spacerun:yes">&nbsp;
</span>փաթեթը<span style="mso-spacerun:yes">&nbsp; </span>ներկայացնելուց հետո:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">4.3<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">4.2-րդ կետով սահմանված ժամկետում
&lt;&lt;Գնորդի&gt;&gt; կողմից &lt;&lt;Ապրանքի&gt;&gt; լրիվ արժեքը<span style="mso-spacerun:yes">&nbsp; </span>չվճարելու դեպքում՝ Վաճառողն իրավուքն ունի պահանջել
տուժանք ժամկետանց օրվա համար չվճարված գումարի 0.0001%-ի չափով:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;line-height:normal;
tab-stops:right 468.0pt"><span style="font-size:10.0pt;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;line-height:normal;
tab-stops:right 468.0pt"><span style="font-size:10.0pt;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraphCxSpLast" align="center" style="text-align:center;
text-indent:-18.0pt;line-height:normal;mso-list:l0 level1 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:
Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:Sylfaen">Անհաղթահարելի ուժի ազդեցություն (ՖՈՐՍ-ՄԱԺՈՐ)<o:p></o:p></span></b></p>

<p class="MsoNormal" style="margin-left:18.0pt;text-align:justify;line-height:
normal;tab-stops:right 468.0pt"><span style="font-size:10.0pt;font-family:Sylfaen">Սույն
պայամանագրով պարտավորություններն ամբողջությամբ կամ մասնակիորեն չկատարելու համար
&lt;&lt;Կողմերն&gt;&gt; ազատվում են պատասխանատվությունից, եթե դա եղել է անհաղթահարելի
ուժի ազդեցության հետևանքով, որը ծագել է սույն պայմանագիրը կնքելուց հետո և, որը
&lt;&lt;Կողմերը&gt;&gt; չեն կարող կանխատեսել կամ կանխարգելել: Այդպիսիք են՝ երկրաշարժը,
կարկտահարությունը, ցրտահարությունը, ջրհեղեղը, հրդեհը և բնական աղետները, պատերազմը,
ռազմական և արտակարգ դրություն հայտարարելը, քաղաքական հուզումները, գործադուլները,
հաղորդակցության միջոցների աշխատանքի դադարեցումը, պետական մարմինների ակտերը և այլ,
որոնք անհնարին են դարձնում սույն պայմանագրով սահմանված պարտավորությունների կատարումը:
Եթե արտակարգ ուժի ազդոցությունը շարունակվում է 3 (երեք) ամսից ավելի, ապա կողմերից
յուրաքանչյուրն իրավունք ունի լուծել պայմանագիրը՝ այդ մասին նախապես տեղյակ պահելով
մյուս կողմին:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpFirst" align="center" style="text-align:center;
text-indent:-18.0pt;line-height:normal;mso-list:l0 level1 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:
Sylfaen;mso-fareast-font-family:Sylfaen;mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:Sylfaen">Եզրափակիչ դրույթներ<o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;line-height:normal;
tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">6.1<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">Սույն պայմանագրի կապակցությամբ ծագած
վեճերը լուծվում են բանակցությունների միջոցով: Համաձայնություն ձեռք չբերելու դեպքում
վեճերը լուծվում են դատական կարգով:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">6.2<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">Սույն պայմանագրի լուծումը կամ ցանկացած
փոփոխություն կատարվում է &lt;&lt;Կողմերի&gt;&gt; միջև գրավոր փաստաթուղթ ստորագրելու
միջոցով:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">6.3<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">Սույն պայմանագիրը կնքվում է մինչև
4.2 կետով նախատեսված պարտականությունների կատարումը և ուժի մեջ է կնքման պահից:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-18.0pt;
line-height:normal;mso-list:l0 level2 lfo1;tab-stops:right 468.0pt"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Sylfaen;mso-fareast-font-family:Sylfaen;
mso-bidi-font-family:Sylfaen"><span style="mso-list:Ignore">6.4<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:10.0pt;font-family:Sylfaen">Սույն պայմանագիրը կնքված է հայերեն
լեզվով, 2 օրինակից, որոնք ունեն հավասարազոր իրավաբանական ուժ: Յուրաքանչյուր
&lt;&lt;Կողմին&gt;&gt; տրվում է մեկական օրինակ:<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="text-align:justify;tab-stops:right 468.0pt"><span style="font-family:Sylfaen"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="line-height:normal;tab-stops:325.8pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:
Sylfaen"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="mso-spacerun:yes">&nbsp;</span>&lt;&lt;Գնորդ&gt;&gt;<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>&lt;&lt;Վաճառող&gt;&gt;<o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpFirst" style="line-height:normal;tab-stops:367.2pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen">&lt;&lt;</span></b><span style="font-size:10.0pt;font-family:Sylfaen">
ՄԻՋՆԱԲԵՐԴ ԱԼԿՈ</span><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:Sylfaen"> &gt;&gt; ՍՊԸ<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;  </span><span style="mso-spacerun:yes">&nbsp;</span><span style="  display:block;
    width:250px;
    word-wrap:break-word;
margin-left:410px;
 position: relative;
    top: -20px;


">Ա.Ա.Հ.՝ {{$customer['name']}} {{$customer['surname']}} {{$customer['third']}}</span> <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:324.6pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:324.6pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:324.6pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen">ՀՎՀՀ՝  <span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span>00</span></b> <b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen">179206<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; </span></span></b><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen;mso-bidi-font-family:Sylfaen">Հեռախոս՝ {{$customer['phone']}}</span></b><b style="mso-bidi-font-weight:
normal"><span style="font-size:9.0pt;font-family:Sylfaen"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:324.6pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:256.5pt 287.4pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen">Հասցե՝ ք. Երևան, Օրբելի Եղբ. փող., 63/1 25<span style="mso-tab-count:1;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; </span>  <span style="  display:block;
    width:250px;
    word-wrap:break-word;
margin-left:410px;
 position: relative;
    top: -20px;


">Հասցե՝  {{$customer['address']}} </span><o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:256.5pt 287.4pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:287.4pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen">Հեռ.<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>010.427333, 091.426359<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Անձնագիր/ (Նույն. քարտ) {{$customer['passport_seria']}}<span style="mso-spacerun:yes">&nbsp; </span><o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:10.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:261.0pt 270.0pt 288.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen">Տնօրեն՝ Գագիկ Ավետիսյան<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="  display:block;
    width:250px;
    word-wrap:break-word;
margin-left:410px;
 position: relative;
    top: -20px;


">Տրվ.՝
                {{$customer['passport_koxmic']}} կողմից, {{$passport}}</span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:right 468.0pt"><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;font-family:
Sylfaen"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraphCxSpMiddle" style="line-height:normal;tab-stops:right 468.0pt"><v:line id="Straight_x0020_Connector_x0020_2" o:spid="_x0000_s1027" style="position:absolute;
 left:0;text-align:left;flip:y;z-index:251659264;visibility:visible;
 mso-wrap-style:square;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text" from="36.3pt,11.65pt" to="169.5pt,14.05pt" o:gfxdata="UEsDBBQABgAIAAAAIQApm/tGBAEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRzU7DMBCE
70i8g+UrSpxyQAgl6YGfI3AoD7DYm8TCsS2vW9q3Z5OmF1RVSFws2+uZ+TSu1/vRiR0mssE3clVW
UqDXwVjfN/Jj81LcS0EZvAEXPDbygCTX7fVVvTlEJMFqT40cco4PSpEecAQqQ0TPky6kETIfU68i
6C/oUd1W1Z3SwWf0uciTh2zrJ+xg67J43vP1kYTlUjwe301RjYQYndWQGVRNU3VWl9DRBeHOm190
xUJWsnI2p8FGulkS3riaZA2Kd0j5FUbmUNrZ+BkgGWUSfHNRdNqsysvYZ9JD11mNJujtyI2Ui+Pf
4jO3jWpe/58825xy1fy77Q8AAAD//wMAUEsDBBQABgAIAAAAIQCtMD/xwQAAADIBAAALAAAAX3Jl
bHMvLnJlbHOEj80KwjAQhO+C7xD2btN6EJGmvYjgVfQB1mTbBtskZOPf25uLoCB4m2XYb2bq9jGN
4kaRrXcKqqIEQU57Y12v4HTcLdYgOKEzOHpHCp7E0DbzWX2gEVN+4sEGFpniWMGQUthIyXqgCbnw
gVx2Oh8nTPmMvQyoL9iTXJblSsZPBjRfTLE3CuLeVCCOz5CT/7N911lNW6+vE7n0I0KaiPe8LCMx
9pQU6NGGs8do3ha/RVXk5iCbWn4tbV4AAAD//wMAUEsDBBQABgAIAAAAIQBrEh0AtwEAAEYEAAAf
AAAAY2xpcGJvYXJkL2RyYXdpbmdzL2RyYXdpbmcxLnhtbKRTy27bMBC8F+g/ELw3kp3ETYTIObht
LkUbxGnvC4qUiJBLgWQU+e+71MM2jCAtmhuXnJmdWZI3t701rJM+aIclX5zlnEkUrtJYl/zX47dP
V5yFCFiBcShLvpOB364/friBovbQNlowUsBQQMmbGNsiy4JopIVw5lqJdKactxCp9HVWeXghZWuy
ZZ6vMgsa+fog9QUisGev/0PKOPEkqw1gB4EkjSiOdyaPRrxfGQrs7ny7be99ci5+dPee6arkNDkE
SyPi2XQwwajMTlj1QaBX3ia8U4r1dAPnny8vFqS1K/n15ZKKfNSTfWQiAVbXixVtMkGI8/ziajoX
zc+/KIjm65saZHI0Q4sjg6LHbZscYrdJy9Pcyzn3NnrQdRPZxiFKEZ1ny/0sJuo8iyOlMCmm3kwZ
3f6mkMOreN9I9nGgaH2Id9JZlhYlNxrl0AC67yGOlmbIcFezpRB3RqboBh+koltO8x+YwxuXG+NZ
B6bk1dMiJaWWAzJRlDZmT8rfJk3YRJNK0ej+lbhHDx0dHohWo/OvdY39bFWN+DHxmJQCTNednfyg
ATX9+PRNj+v1HwAAAP//AwBQSwMEFAAGAAgAAAAhAGfkJTNBBgAAzxkAABoAAABjbGlwYm9hcmQv
dGhlbWUvdGhlbWUxLnhtbOxZzW4bNxC+F+g7LPbeWLL1ExuRA1uS4zZWEkRKihypXWqXMXe5ICk7
uhXJsUCBomnRQwP01kPRNkAC9JI+jdsUbQrkFTrk/oiUqNoxfDCKyICxO/vNcDgz+w3JvXb9UUK9
I8wFYWnHr1+p+R5OAxaSNOr490Z7H131PSFRGiLKUtzxZ1j417c//OAa2gooycYM8XAU4wR7YCgV
W6jjx1JmW2trIgAxEldYhlN4NmE8QRJuebQWcnQMAyR0bb1Wa60liKT+NliUylCfwr9UCiUIKB8q
M9hLUQKj355MSIA1NjysK4SYiS7l3hGiHR9shux4hB9J36NISHjQ8Wv6569tX1tDW4USlSt0Db09
/Sv0CoXwcF2PyaNxNWij0Wy0dir7GkDlMq7f7rf6rcqeBqAggJnmvpg2m7ubu71mgTVA+aXDdq/d
26hbeMP+xpLPO031Z+E1KLffWMLv7XUhihZeg3J8cwnfaLTXuw0Lr0E5vrWEb9d2eo22hdegmJL0
cAlda7Y2uuVsK8iE0X0nfLPZ2GuvF8bnKKiGqrrUEBOWylW1lqCHjO8BQAEpkiT15CzDExRATXYR
JWNOvAMSxVINg7YwMp7nokAsidSIngg4yWTH/yRDqW9A3r766e2rF97J45cnj389efLk5PEvuSFL
ax+lkan15ocv/3n2mff3i+/fPP3ajRcm/o+fP//9t6/cQHiJ5pN8/c3zP18+f/3tF3/9+NQB3+Fo
bMJHJMHCu4WPvbssgYnpqNie4zF/N41RjIipsZNGAqVIjeKw35exhb41QxQ5cLvYjuB9DiTiAt6Y
PrQcHsZ8KonD4s04sYADxugu484o3FRjGWEeTdPIPTifmri7CB25xu6i1Mpvf5oBexKXyW6MLTfv
UJRKFOEUS089Y4cYO2b3gBArrgMScCbYRHoPiLeLiDMkIzK2qmmutE8SyMvM5SDk24rN4L63y6hr
1j18ZCPhrUDU4fwIUyuMN9BUosRlcoQSagb8AMnY5eRwxgMT1xcSMh1hyrx+iIVw6dzmMF8j6TeB
QNxpH9BZYiO5JIcumweIMRPZY4fdGCWZCzskaWxiPxaHUKLIu8OkCz5g9hui7iEPKF2Z7vsEW+k+
nQ3uAXeaLs0LRD2Zckcub2Bm1e9wRicIa6oBarcYOyHpqfSdj3BxxA1U+fq7Zw6/Lytl73DifGf2
F4h6FW6RnruMh+Tys3MPTdM7GF6I5Rb1npzfk7P/vyfnVe/zxVPynIWBoNViMF9u68V3snLtPSGU
DuWM4gOhl98Cek+4B0Klp/eYuNqLZTFcqjcZBrBwEUdax+NMfkpkPIxRBkv3uq+MRKIwHQkvYwK2
jFrstK3wdJoMWJhvOet1tb3MyUMgOZfXmpUctgsyR7fa821UZV57G+ntbumA0n0XJ4zBbCc2HE60
S6EKkt5cQ9AcTuiZXYgXmw4vrirzZaqWvADXqqzA4siDJVXHbzZABZRgz4QoDlWe8lSX2dXJvMhM
rwqmVQE1OMMoKmCe6U3l68rpqdnlpXaGTFtOGOVmO6Ejo3uYiFGIi+pU0rO48a653pyn1HJPhaKI
heFG++p/eXHeXIPeIjfQ1GQKmnrHHb+10YSSCVDW8SewdYfLJIPaEWpRi2gEh16B5PkLfx5mybiQ
PSTiPOCadHI2SIjE3KMk6fhq+lUaaKo5RPtWXwdCuLTObQKtXDbnIOl2kvFkggNppt2QqEjnt8Dw
OVc4n2r184OVJptCuodxeOyN6ZTfRVBizXZdBTAkAs536nk0QwJHkhWRzetvoTEVtGueCeoayuWI
ZjEqOopJ5jlcU3nljr6rYmDcFXOGgBohKRrhOFIN1gyq1U2rrpH7sLLrnq6kImeQ5rxnWqyiuqab
xawRyjawEMvzNXnDqzLE0C7NDp9T9yLlbpZct7BOqLoEBLyKn6PrnqEhGK7NB7NcUx4v07Di7EJq
945ygqe4dpYmYbB+qzS7ELeqRziHA+G5Oj/oLVYtiCblulJH2vV5YYAybxzVOz4c8cMJxCO4go8E
PsjWlWxdyeAKTv6hXeTH9R2/uCgl8DyXVJiNUrJRYhqlpFFKmqWkWUpapaTle/pcG76lqCNt3yuP
raGHFcfcxdrC/gaz/S8AAAD//wMAUEsDBBQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAY2xpcGJv
YXJkL2RyYXdpbmdzL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxzhI/NCsIwEITvgu8Q9m7SehCRJr2I
0KvUBwjJNi02PyRR7Nsb6EVB8LIws+w3s037sjN5YkyTdxxqWgFBp7yenOFw6y+7I5CUpdNy9g45
LJigFdtNc8VZ5nKUxikkUigucRhzDifGkhrRykR9QFc2g49W5iKjYUGquzTI9lV1YPGTAeKLSTrN
IXa6BtIvoST/Z/thmBSevXpYdPlHBMulFxagjAYzB0pXZ501LV2BiYZ9/SbeAAAA//8DAFBLAQIt
ABQABgAIAAAAIQApm/tGBAEAAB4CAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10u
eG1sUEsBAi0AFAAGAAgAAAAhAK0wP/HBAAAAMgEAAAsAAAAAAAAAAAAAAAAANQEAAF9yZWxzLy5y
ZWxzUEsBAi0AFAAGAAgAAAAhAGsSHQC3AQAARgQAAB8AAAAAAAAAAAAAAAAAHwIAAGNsaXBib2Fy
ZC9kcmF3aW5ncy9kcmF3aW5nMS54bWxQSwECLQAUAAYACAAAACEAZ+QlM0EGAADPGQAAGgAAAAAA
AAAAAAAAAAATBAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAnGZG
QbsAAAAkAQAAKgAAAAAAAAAAAAAAAACMCgAAY2xpcGJvYXJkL2RyYXdpbmdzL19yZWxzL2RyYXdp
bmcxLnhtbC5yZWxzUEsFBgAAAAAFAAUAZwEAAI8LAAAAAA==
" strokecolor="black [3200]" strokeweight=".5pt">
        <v:stroke joinstyle="miter">
        </v:stroke></v:line><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;
font-family:Sylfaen"><span style="mso-spacerun:yes">&nbsp; </span><o:p></o:p></span></b></p>

<p class="MsoListParagraphCxSpLast" style="line-height:normal;tab-stops:81.6pt 96.0pt 310.2pt"><v:line id="Straight_x0020_Connector_x0020_3" o:spid="_x0000_s1026" style="position:absolute;
 left:0;text-align:left;z-index:251660288;visibility:visible;mso-wrap-style:square;
 mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text" from="259.05pt,-.15pt" to="395.25pt,-.15pt" o:gfxdata="UEsDBBQABgAIAAAAIQApm/tGBAEAAB4CAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKSRzU7DMBCE
70i8g+UrSpxyQAgl6YGfI3AoD7DYm8TCsS2vW9q3Z5OmF1RVSFws2+uZ+TSu1/vRiR0mssE3clVW
UqDXwVjfN/Jj81LcS0EZvAEXPDbygCTX7fVVvTlEJMFqT40cco4PSpEecAQqQ0TPky6kETIfU68i
6C/oUd1W1Z3SwWf0uciTh2zrJ+xg67J43vP1kYTlUjwe301RjYQYndWQGVRNU3VWl9DRBeHOm190
xUJWsnI2p8FGulkS3riaZA2Kd0j5FUbmUNrZ+BkgGWUSfHNRdNqsysvYZ9JD11mNJujtyI2Ui+Pf
4jO3jWpe/58825xy1fy77Q8AAAD//wMAUEsDBBQABgAIAAAAIQCtMD/xwQAAADIBAAALAAAAX3Jl
bHMvLnJlbHOEj80KwjAQhO+C7xD2btN6EJGmvYjgVfQB1mTbBtskZOPf25uLoCB4m2XYb2bq9jGN
4kaRrXcKqqIEQU57Y12v4HTcLdYgOKEzOHpHCp7E0DbzWX2gEVN+4sEGFpniWMGQUthIyXqgCbnw
gVx2Oh8nTPmMvQyoL9iTXJblSsZPBjRfTLE3CuLeVCCOz5CT/7N911lNW6+vE7n0I0KaiPe8LCMx
9pQU6NGGs8do3ha/RVXk5iCbWn4tbV4AAAD//wMAUEsDBBQABgAIAAAAIQDNvL8ZsQEAAAwEAAAf
AAAAY2xpcGJvYXJkL2RyYXdpbmdzL2RyYXdpbmcxLnhtbKRTy27cIBTdV+o/IPaJ7Xl0EiueLKZt
NlUbZdIPuMJgo+CLBdT1/H0BP2YyGkVVskE8zuOeC9zd940iHTdWaixodp1SwpHpUmJV0N/P369u
KLEOsASlkRf0wC29337+dAd5ZaCtJSNeAW0OBa2da/MksazmDdhr3XL0Z0KbBpxfmiopDfz1yo1K
Fmn6JWlAIt0epb6CA/LHyHdIKc1eeLkD7MB6ScXy052xRsU+rgw5dg+m3bePJlTOfnaPhsiyoL5z
CI1vEU3GgxHml8kZqzoK9MI0Aa+FIH1UOYQxavDeETZssuMuq39dwLL62wW0Nx4M/OTElPW4b4Mr
drswPc+ynLLsnQFZ1Y7sNCJnThuynPON1CnfiZIdFc/CrRbparlcU+LD3K4XN9lm/Tpotlncbla+
k3PcOQDkrbHugeuGhElBlUQeHw90P6wbipggseNTEdYdFA9hFT5x4e/KdzSLzPhS+U4Z0oEqaPmS
hXK8ZUQGipBKzaT0bdKIDTQuhG/W/xJndHTUeCQ2ErW55Or6qVQx4IfEQ1IfYLzg5OwfRNT4b8Nn
O11v/wEAAP//AwBQSwMEFAAGAAgAAAAhAGfkJTNBBgAAzxkAABoAAABjbGlwYm9hcmQvdGhlbWUv
dGhlbWUxLnhtbOxZzW4bNxC+F+g7LPbeWLL1ExuRA1uS4zZWEkRKihypXWqXMXe5ICk7uhXJsUCB
omnRQwP01kPRNkAC9JI+jdsUbQrkFTrk/oiUqNoxfDCKyICxO/vNcDgz+w3JvXb9UUK9I8wFYWnH
r1+p+R5OAxaSNOr490Z7H131PSFRGiLKUtzxZ1j417c//OAa2gooycYM8XAU4wR7YCgVW6jjx1Jm
W2trIgAxEldYhlN4NmE8QRJuebQWcnQMAyR0bb1Wa60liKT+NliUylCfwr9UCiUIKB8qM9hLUQKj
355MSIA1NjysK4SYiS7l3hGiHR9shux4hB9J36NISHjQ8Wv6569tX1tDW4USlSt0Db09/Sv0CoXw
cF2PyaNxNWij0Wy0dir7GkDlMq7f7rf6rcqeBqAggJnmvpg2m7ubu71mgTVA+aXDdq/d26hbeMP+
xpLPO031Z+E1KLffWMLv7XUhihZeg3J8cwnfaLTXuw0Lr0E5vrWEb9d2eo22hdegmJL0cAlda7Y2
uuVsK8iE0X0nfLPZ2GuvF8bnKKiGqrrUEBOWylW1lqCHjO8BQAEpkiT15CzDExRATXYRJWNOvAMS
xVINg7YwMp7nokAsidSIngg4yWTH/yRDqW9A3r766e2rF97J45cnj389efLk5PEvuSFLax+lkan1
5ocv/3n2mff3i+/fPP3ajRcm/o+fP//9t6/cQHiJ5pN8/c3zP18+f/3tF3/9+NQB3+FobMJHJMHC
u4WPvbssgYnpqNie4zF/N41RjIipsZNGAqVIjeKw35exhb41QxQ5cLvYjuB9DiTiAt6YPrQcHsZ8
KonD4s04sYADxugu484o3FRjGWEeTdPIPTifmri7CB25xu6i1Mpvf5oBexKXyW6MLTfvUJRKFOEU
S089Y4cYO2b3gBArrgMScCbYRHoPiLeLiDMkIzK2qmmutE8SyMvM5SDk24rN4L63y6hr1j18ZCPh
rUDU4fwIUyuMN9BUosRlcoQSagb8AMnY5eRwxgMT1xcSMh1hyrx+iIVw6dzmMF8j6TeBQNxpH9BZ
YiO5JIcumweIMRPZY4fdGCWZCzskaWxiPxaHUKLIu8OkCz5g9hui7iEPKF2Z7vsEW+k+nQ3uAXea
Ls0LRD2Zckcub2Bm1e9wRicIa6oBarcYOyHpqfSdj3BxxA1U+fq7Zw6/Lytl73DifGf2F4h6FW6R
nruMh+Tys3MPTdM7GF6I5Rb1npzfk7P/vyfnVe/zxVPynIWBoNViMF9u68V3snLtPSGUDuWM4gOh
l98Cek+4B0Klp/eYuNqLZTFcqjcZBrBwEUdax+NMfkpkPIxRBkv3uq+MRKIwHQkvYwK2jFrstK3w
dJoMWJhvOet1tb3MyUMgOZfXmpUctgsyR7fa821UZV57G+ntbumA0n0XJ4zBbCc2HE60S6EKkt5c
Q9AcTuiZXYgXmw4vrirzZaqWvADXqqzA4siDJVXHbzZABZRgz4QoDlWe8lSX2dXJvMhMrwqmVQE1
OMMoKmCe6U3l68rpqdnlpXaGTFtOGOVmO6Ejo3uYiFGIi+pU0rO48a653pyn1HJPhaKIheFG++p/
eXHeXIPeIjfQ1GQKmnrHHb+10YSSCVDW8SewdYfLJIPaEWpRi2gEh16B5PkLfx5mybiQPSTiPOCa
dHI2SIjE3KMk6fhq+lUaaKo5RPtWXwdCuLTObQKtXDbnIOl2kvFkggNppt2QqEjnt8DwOVc4n2r1
84OVJptCuodxeOyN6ZTfRVBizXZdBTAkAs536nk0QwJHkhWRzetvoTEVtGueCeoayuWIZjEqOopJ
5jlcU3nljr6rYmDcFXOGgBohKRrhOFIN1gyq1U2rrpH7sLLrnq6kImeQ5rxnWqyiuqabxawRyjaw
EMvzNXnDqzLE0C7NDp9T9yLlbpZct7BOqLoEBLyKn6PrnqEhGK7NB7NcUx4v07Di7EJq945ygqe4
dpYmYbB+qzS7ELeqRziHA+G5Oj/oLVYtiCblulJH2vV5YYAybxzVOz4c8cMJxCO4go8EPsjWlWxd
yeAKTv6hXeTH9R2/uCgl8DyXVJiNUrJRYhqlpFFKmqWkWUpapaTle/pcG76lqCNt3yuPraGHFcfc
xdrC/gaz/S8AAAD//wMAUEsDBBQABgAIAAAAIQCcZkZBuwAAACQBAAAqAAAAY2xpcGJvYXJkL2Ry
YXdpbmdzL19yZWxzL2RyYXdpbmcxLnhtbC5yZWxzhI/NCsIwEITvgu8Q9m7SehCRJr2I0KvUBwjJ
Ni02PyRR7Nsb6EVB8LIws+w3s037sjN5YkyTdxxqWgFBp7yenOFw6y+7I5CUpdNy9g45LJigFdtN
c8VZ5nKUxikkUigucRhzDifGkhrRykR9QFc2g49W5iKjYUGquzTI9lV1YPGTAeKLSTrNIXa6BtIv
oST/Z/thmBSevXpYdPlHBMulFxagjAYzB0pXZ501LV2BiYZ9/SbeAAAA//8DAFBLAQItABQABgAI
AAAAIQApm/tGBAEAAB4CAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsB
Ai0AFAAGAAgAAAAhAK0wP/HBAAAAMgEAAAsAAAAAAAAAAAAAAAAANQEAAF9yZWxzLy5yZWxzUEsB
Ai0AFAAGAAgAAAAhAM28vxmxAQAADAQAAB8AAAAAAAAAAAAAAAAAHwIAAGNsaXBib2FyZC9kcmF3
aW5ncy9kcmF3aW5nMS54bWxQSwECLQAUAAYACAAAACEAZ+QlM0EGAADPGQAAGgAAAAAAAAAAAAAA
AAANBAAAY2xpcGJvYXJkL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAnGZGQbsAAAAk
AQAAKgAAAAAAAAAAAAAAAACGCgAAY2xpcGJvYXJkL2RyYXdpbmdzL19yZWxzL2RyYXdpbmcxLnht
bC5yZWxzUEsFBgAAAAAFAAUAZwEAAIkLAAAAAA==
" strokecolor="black [3200]" strokeweight=".5pt">
        <v:stroke joinstyle="miter">
        </v:stroke></v:line><b style="mso-bidi-font-weight:normal"><span style="font-size:9.0pt;
font-family:Sylfaen">(ստորագրություն)<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="mso-spacerun:yes">&nbsp;</span>(ստորագրություն)<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></b></p>

<!--EndFragment-->


</body>
</html>
