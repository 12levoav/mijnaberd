<?php

namespace App\Http\Controllers\Dashboard;

use App\Auto;
use App\AutoModel;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartRequest;
use App\Part;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


     $users = User::all();
     $customers= Customer::paginate(10);

        return view('dashboard.users.index', compact('users','customers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $pdf = PDF::loadView('pdf.vors');
        return $pdf->download('invoice.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $part = Part::find($id);

        if (!$part) {
            abort(404);
        }


        return view('dashboard.parts.edit', compact('part'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PartRequest $request, $id)
    {
        $part = Part::find($id);
        if (!$part) {
            abort(404);
        }
        $part->update($request->all());

        return redirect()->route('parts.index', ['car_id' => AutoModel::where('id', $part->model_id)->first()->auto_id, 'model_id' => $part->model_id, 'modification_id' => $part->modification_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();
        return redirect()->route('users.index');
    }

}
