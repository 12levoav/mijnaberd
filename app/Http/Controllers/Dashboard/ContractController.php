<?php

namespace App\Http\Controllers\Dashboard;

use App\Customer;
use App\Http\Controllers\Controller;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;

class ContractController extends Controller
{
    public function create()
    {
        return view('dashboard.contracts.create');

    }
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('dashboard.contracts.edit',compact('customer'));

    }

    public function store(Request $request)
    {

        $date = $request->date;
        $passport =$request->passport_date;
        $customer = Customer::create($request->all());
        $pdf = PDF::loadView('pdf.mrgi-paymanagir', array('customer' => $customer,'date'=>$date,'passport'=>$passport));
        $pdf2 = PDF::loadView('pdf.gnman-akt', array('customer' => $customer,'date'=>$date,'passport'=>$passport));
        $pdf3 = PDF::loadView('pdf.eo', array('customer' => $customer,'date'=>$date,'passport'=>$passport));
        Storage::disk('public')->put('pdf/mrgi_paymanagir' . $customer->id . '.pdf', $pdf->output());
        Storage::disk('public')->put('pdf/gnman_akt' . $customer->id . '.pdf', $pdf2->output());
        Storage::disk('public')->put('pdf/eo' . $customer->id . '.pdf', $pdf3->output());
        $customer['mrgi_paymanagir'] = 'pdf/mrgi_paymanagir' . $customer->id . '.pdf';
        $customer['gnman_akt'] = 'pdf/gnman_akt' . $customer->id . '.pdf';
        $customer['eo'] = 'pdf/eo' . $customer->id . '.pdf';
        $customer['date'] = $date;
        $customer['passport_date'] = $passport;
        $customer->save();
//        $zipper = new Zipper();
//        $zipper->make('public/storage/zips/zip'.$customer->id .'.zip')->add(['storage/pdf/eo'.$customer->id .'.pdf','storage/pdf/gnman_akt'.$customer->id .'.pdf','storage/pdf/mrgi_paymanagir'.$customer->id .'.pdf']);

        return redirect()->route('users.index');

    }
 public function update(Request $request,$id)
    {
            $customer = Customer::find($id);
        if (!$customer) {
            abort(404);
        }
        $date = $request->date;
        $passport = $request->passport_date;
        $customer->update($request->all());

        $pdf = PDF::loadView('pdf.mrgi-paymanagir', array('customer' => $customer,'date'=>$date,'passport'=>$passport));
        $pdf2 = PDF::loadView('pdf.gnman-akt', array('customer' => $customer,'date'=>$date,'passport'=>$passport));
        $pdf3 = PDF::loadView('pdf.eo', array('customer' => $customer,'date'=>$date,'passport'=>$passport));
        Storage::disk('public')->put('pdf/mrgi_paymanagir' . $customer->id . '.pdf', $pdf->output());
        Storage::disk('public')->put('pdf/gnman_akt' . $customer->id . '.pdf', $pdf2->output());
        Storage::disk('public')->put('pdf/eo' . $customer->id . '.pdf', $pdf3->output());
        $customer['mrgi_paymanagir'] = 'pdf/mrgi_paymanagir' . $customer->id . '.pdf';
        $customer['gnman_akt'] = 'pdf/gnman_akt' . $customer->id . '.pdf';
        $customer['eo'] = 'pdf/eo' . $customer->id . '.pdf';
        $customer['date'] = $date;
        $customer['passport_date'] = $passport;
        $customer->save();
//        $zipper = new Zipper();
//        $zipper->make('public/storage/zips/zip'.$id.'.zip')->add(['storage/pdf/eo'.$id.'.pdf','storage/pdf/gnman_akt'.$id.'.pdf','storage/pdf/mrgi_paymanagir'.$id.'.pdf']);

        return redirect()->route('users.index');

    }

    public function destroy($id)
    {
        $customer = Customer::find($id);

        $customer->delete();
        return redirect()->route('users.index');
    }
    public  function zip($id)
    {
          return response()->download('public/storage/zips/zip'.$id.'.zip');
    }
}
