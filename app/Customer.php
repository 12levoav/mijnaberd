<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ayl_texekutyunner','havelvac','himqy','nshanakutyan_cackagir','hashvarman_cackagir','txtakcox_hashiv','gnman_akt','gumar','gin','qanak','chap','apranq_name','passport_koxmic','passport_date','name','surname', 'place','date','address','phone','hvhh','passport_seria','third', 'mrgi_paymanagir', 'eo', 'gnman_akt'];


}
