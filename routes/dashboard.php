<?php

Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
Route::get('/contract', ['as' => 'dashboard', 'uses' => 'ContractController@create']);
Route::get('/contract/{id}', ['as' => 'edit', 'uses' => 'ContractController@edit']);
Route::post('/contract/store', ['as' => 'store', 'uses' => 'ContractController@store']);
Route::get('/contract/zip/{id}', ['as' => 'zip', 'uses' => 'ContractController@zip']);
Route::put('/contract/update/{id}', ['as' => 'customer.update', 'uses' => 'ContractController@update']);
Route::delete('/contract/delete/{id}', ['as' => 'customers.destroy', 'uses' => 'ContractController@destroy']);
Route::get('/users/pdf', ['as' => 'pdf', 'uses' => 'UserController@pdf']);
Route::resource('users','UserController');


