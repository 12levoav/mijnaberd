<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name')->nullable();
            $table->text('third')->nullable();
            $table->text('surname')->nullable();
            $table->text('place')->nullable();
            $table->text('date')->nullable();
            $table->text('address')->nullable();
            $table->text('phone')->nullable();
            $table->text('hvhh')->nullable();
            $table->text('passport_seria')->nullable();
            $table->text('passport_date')->nullable();
            $table->text('passport_koxmic')->nullable();
            $table->text('mrgi_paymanagir')->nullable();
            $table->text('eo')->nullable();
            $table->text('apranq_name')->nullable();
            $table->text('chap')->nullable();
            $table->text('qanak')->nullable();
            $table->text('gin')->nullable();
            $table->text('gumar')->nullable();
            $table->text('gnman_akt')->nullable();
            $table->text('txtakcox_hashiv')->nullable();
            $table->text('hashvarman_cackagir')->nullable();
            $table->text('nshanakutyan_cackagir')->nullable();
            $table->text('himqy')->nullable();
            $table->text('havelvac')->nullable();
            $table->text('ayl_texekutyunner')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
